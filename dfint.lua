

math.randomseed(os.time())

function io.readAll(filename)
  local doc= io.open(filename,"r")
  if not doc then error("File "..filename.." not found.") end
  local r= {}
  local ln= doc:read()
  while ln do
    table.insert(r,ln)
    ln= doc:read()
  end
  doc:close()
  return r
end
function string.split(self,sep)
  sep= sep or ' '
  self= self..sep
  local tab= {}
  local word= ""
  
  for i=1,#self do
    local ch= self:sub(i,i)
    if ch == sep then
      if #word > 0 then
        table.insert(tab,word.."")
        word= ''
      end
    else
      word= word..ch
    end
  end
  
  return tab
end
function string.rmvChar(self,ch,r)
  if r then -- left to right
    local l= #self
    while self:sub(l,l) == ch do
      self= self:sub(1,l-1)
      l= l-1
    end
  else
    while self:sub(1,1) == ch do
      self= self:sub(2,#self)
    end
  end
  return self
end
function string.rmvCharN(self,ch,r)
  if r then
    local l= #self
    while self:sub(l,l) ~= ch do
      self= self:sub(1,l-1)
      l= l-1
    end
  else
    while self:sub(1,1) ~= ch do
      self= self:sub(2,#self)
    end
  end
  return self
end

function table.findrmv(tab,item)
  for i=1,#tab do
    if tab[i] == item then
      return table.remove(tab,i),i
    end
  end
end
function limit(x,min,max)
  if x < min then return min
  elseif x > max then return max
  end
  return x
end
function overflow(x,min,max)
  while x < min do
    x= x-min+1+max
  end
  while x > max do
    x= x-max-1+min
  end
  return x
end




dfcmd= {
  select= {0,
    function(self,int)
      local a= 1
      if not self[1] then int:error("")
      elseif self[1] == "." then int.sel,a= _G,2
      end
      for i=a,#self do
        local it= self[i]
        if not int.sel[it] then int:error("Could not ") end
        int.sel= int.sel[it]
      end
    end
  },
  is= {1,
    function(self,int)
      local v= int.sel[self[1]]
      if not self[2] then
        -- Return type
        if not v then table.insert(int.stack,false)
        else table.insert(int.stack,type(v))
        end
      elseif self[2] == "here" then
        if v then table.insert(int.stack,true)
        else table.insert(int.stack,false)
        end
      elseif dftypes[self[2]] then
        if type(v) == self[2] or type(v) == dftypes[self[2]] then
          table.insert(int.stack,true)
        else
          table.insert(int.stack,false)
        end
      else
        int.error("Unknown type: "..self[2])
      end
    end
  },
  
  bool= {2,
    function(self,int)
      local a= self[2]
      if a == "0" or a == "F" or a == "false" then int.sel[self[1]]= false
      else int.sel[self[1]]= true
      end
    end
  },
  num= {2,
    function(self,int)
      local v= tonumber(self[2])
      if not v then int.error("Argument 2 is not a number.")
      else int.sel[self[1]]= v
      end
    end
  },
  str= {2,
    function(self,int)
      local v= int.code[int.lni]
      int.sel[self[1]]= v:sub(5,#v):rmvCharN(" "):rmvChar(" ")
    end
  },
  tab= {1,
    function(self,int)
      int.sel[self[1]]= {}
    end
  },
  txt= {2,
    function(self,int)
      local v= int.code[int.lni]
      local r= v:sub(5,#v):rmvCharN(" "):rmvChar(" ")
      
      while true do
        int.lni= int.lni+1
        if int.lni > int.codelen then
          int.eof=true
          int.lni= int.lni-1
          return 1
        end
        int.ln= int.code[int.lni]
        if int.ln == "end" then
          int.sel[self[1]]= r
          break
        else
          r= r.."\n"..int.ln
        end
      end
    end
  },
  
  log={1,
    function(self,int)
      local v= int.code[int.lni]
      v= v:sub(5,#v)
      if v:sub(1,1) == "." then
        int:logit(int.sel[v:sub(2,#v)])
      elseif v:sub(1,1) == ":" then
        local v= tonumber(v:sub(2,#v))
        if not v then int:error("Argument 1 is not a number")
        else int:logit(int.stack[int.stack[0]-v+1])
        end
      else
        int:logit(v)
      end
    end
  },
  
  pop={0,
    function(self,int)
      if self[1] then
        int.sel[self[1]]= int:popit()
      else
        int:popit()
      end
    end
  },
  push={1,
    function(self,int)
      int:pushit(int.sel[self[1]])
    end
  },
  
  inv={1,
    function(self,int)
      if int.sel[self[1]] then int:pushit(false)
      else int:pushit(true)
      end
    end
  },
  equ={0,
    function(self,int)
      local b= (self[2] and int.sel[self[2]]) or int:popit()
      local a= (self[1] and int.sel[self[1]]) or int:popit()
      int:pushit(a == b)
    end
  },
  grt={0,
    function(self,int)
      local b= (self[2] and int.sel[self[2]]) or int:popit()
      local a= (self[1] and int.sel[self[1]]) or int:popit()
      int:pushit(a > b)
    end
  },
  lst={0,
    function(self,int)
      local b= (self[2] and int.sel[self[2]]) or int:popit()
      local a= (self[1] and int.sel[self[1]]) or int:popit()
      int:pushit(a < b)
    end
  },
  
  calc={0,
    function(self,int,x,y)
      local b= (self[2] and int.sel[self[2]]) or int:popit()
      local a= (self[1] and int.sel[self[1]]) or int:popit()
      local r= a*x + b*y
      if self[1] then
        int.sel[self[1]]= r
      else
        int:pushit(r)
      end
    end
  },
  inc={0,
    function(self,int)
      int:pushit(1)
      dfcmd.calc[2](self,int,1,1)
    end
  },
  add={0,
    function(self,int)
      dfcmd.calc[2](self,int,1,1)
    end
  },
  sub={0,
    function(self,int)
      dfcmd.calc[2](self,int,1,-1)
    end
  },
  mul={0,
    function(self,int)
      local x= (self[2] and int.sel[self[2]]) or int:popit()
      self[2]= 0
      dfcmd.calc[2](self,int,x,1)
    end
  },
  div={0,
    function(self,int)
      local x= (self[2] and int.sel[self[2]]) or int:popit()
      self[2]= 0
      dfcmd.calc[2](self,int,1/x,1)
    end
  },
  
  
  label={1,
    function() end
  },
  jmp={1,
    function(self,int)
      if self[2] then
        self[1]= int.lni+self[1]
      elseif int.labels[self[1]] then
        self[1]= int.labels[self[1]]+1
      end
      int.lni= self[1]-1
      if int.lni < 2 then int.lni= 2 end
    end
  },
  jmpc={1,
    function(self,int)
      if int:popit() then
        dfcmd.jmp[2](self,int)   
      end
    end
  }
}
dftypes= {
  number="num", num="number",
  boolean="bool", bool="boolean",
  string="str", str="string",
  table="tab", tab="table",
  fun="function"
}
dftypes["function"]="fun"

DFInterpreter= {
  header={}, code={}, codelen=0, lni=2, sel=_G, stack={}, ln={}, eof=false,
  log={}, maxlog=20, labels={},
  init= function(self)
    for i=1,self.maxlog do
      log[i]= ""
    end
  end,
  logit= function(self,msg)
    for i=2,self.maxlog do
      self.log[i-1]= self.log[i]
    end
    self.log[1]= msg
    print(msg)
  end,
  pushit= function(self,v)
    self.stack[0]= self.stack[0]+1
    self.stack[self.stack[0]]= v
  end,
  popit= function(self)
    local v= self.stack[self.stack[0]]
    if self.stack[0] >= 0 then
      self.stack[0]= self.stack[0]-1
    end
    return v
  end,
  load= function(self,code)
    self.code= code
    self.header= code[1]:split("|")
    self.codelen= #self.code
    self.lni= 2
    self.sel= _G
    self.stack= {}
    self.stack[0]= 0
    self.ln= {}
    self.eof= false
    self:logit("File "..self.header[1].." loaded and ready.")
    for i=2,#code do
      local a= self.code[i]
      if a:sub(1,5) == "label" then
        local a= a:sub(7,#a):split(";")[1]
        self.labels[a]= i
      end
    end
  end,
  run= function(self)
    if self.lni > self.codelen then
      self.eof=true
      return 1
    end
    self.ln= self.code[self.lni]:split(";")
    if self.ln[1] then
      self.ln= self.ln[1]:split(" ")
      local cmd= dfcmd[self.ln[1]]
      if cmd then
        table.remove(self.ln,1)
        if #self.ln < cmd[1] then
          self:error("This command requires at least "..cmd[1].." arguments.")
        else
          cmd[2](self.ln,self)
        end
      else
        self:logit()
      end
    end
    
    self.lni= self.lni+1
  end,
  error= function(self,msg)
    msg= "\n  "..msg
    msg= self.code[self.lni]..msg
    msg= "DF "..self.header[1].." ["..self.lni.."]: "..msg
    self:logit(msg)
    error(msg)
  end
}



DFInterpreter:load(io.readAll("dftest.txt"))
while not DFInterpreter.eof do
  DFInterpreter:run()
end







